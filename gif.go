package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"math"
	"os"
)

func generateGif(filename string, tour []Position, boardSize int) {
	// Only use three colors
	var palette = []color.Color{
		color.RGBA{161, 126, 98, 255},
		color.RGBA{229, 208, 175, 255},
		color.RGBA{108, 79, 70, 255},
	}

	var images []*image.Paletted
	var delays []int

	// 0.5s delay
	const frameDelay = 50

	// Image size is 500x500 px
	const size = 500

	squareSize := size / boardSize

	// Pawn circle radius
	rad := (float64(squareSize) - 2) / 2
	squareCenter := float64(squareSize) / 2

	for i := 0; i < len(tour); i++ {
		// Create an image for each frame
		img := image.NewPaletted(image.Rect(0, 0, size, size), palette)
		images = append(images, img)
		delays = append(delays, frameDelay)

		// Loop through all pixels in the image and set their color accordingly
		for x := 0; x < size; x++ {
			for y := 0; y < size; y++ {
				// Default to draw a black square
				var r, g, b uint8 = 161, 126, 98

				if (x/squareSize)%2 != (y/squareSize)%2 {
					// This is actually a white square
					r, g, b = 229, 208, 175
				}

				xt := x / squareSize
				yt := y / squareSize

				// Check if it's one of the squares from the tour at this step
				for _, p := range tour[:i+1] {
					if p.X == xt && p.Y == yt {
						// Compute offset within the square
						wx := x - xt*squareSize
						wy := y - yt*squareSize

						// Check if current pixel is within a circle representing pawn position
						dx := squareCenter - float64(wx+1)
						dy := squareCenter - float64(wy+1)
						d := math.Sqrt(dx*dx + dy*dy)
						if d < rad {
							r, g, b = 108, 79, 70
						}
					}
				}

				img.Set(x, y, color.RGBA{r, g, b, 255})
			}
		}
	}

	// Save GIF on disk
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	gif.EncodeAll(file, &gif.GIF{
		Image: images,
		Delay: delays,
	})
}
