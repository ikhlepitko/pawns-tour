package main

import (
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Square represents board square at a time of a particular step in a tour
type Square struct {
	// Number of moves possible from this square
	Moves int

	// Order in the tour, 0 if not part of the tour yet
	StepNumber int
}

// IsInTour checks if a square is part of the current tour
func (s Square) isInTour() bool {
	return s.StepNumber != 0
}

// squareRef provides a reference to a board square together with information about its possition
type squareRef struct {
	Pos Position
	Ref *Square
}

// Board snapshot at a time of a particular step in a tour
type Board struct {
	// Squares representation in matrix form
	Squares [][]Square

	// Pawn steps in a tour
	TourSteps []Position

	// Board side size
	Size int
}

// Position on a board
type Position struct {
	X int
	Y int
}

// Delta represents an offset of an adjacent square to an arbitrary [x, y] position
type Delta struct {
	X int
	Y int
}

// newBoard creates a new board
func newBoard(n int) Board {
	board := Board{}
	board.reset(n)
	return board
}

// rules provides a list of offsets that can be used to compute adjacent squares
func rules() []Delta {
	return []Delta{
		Delta{-2, -2},
		Delta{-2, 2},
		Delta{2, 2},
		Delta{3, 0},
		Delta{0, -3},
		Delta{2, -2},
		Delta{0, 3},
		Delta{-3, 0},
	}
}

// reset board to initial state
func (b *Board) reset(n int) {
	b.Size = n

	b.TourSteps = make([]Position, 0)
	b.Squares = make([][]Square, n)
	deltas := rules()
	for y := 0; y < n; y++ {
		b.Squares[y] = make([]Square, n)

		for x := 0; x < n; x++ {
			sum := 0
			for _, dt := range deltas {
				if x+dt.X >= 0 && x+dt.X < n && y+dt.Y >= 0 && y+dt.Y < n {
					sum++
				}
			}

			b.Squares[y][x].Moves = sum
		}
	}
}

// adjacentSquares returns all squares on a board that are accessible from specified position
func (b Board) adjacentSquares(pos Position) []squareRef {
	res := make([]squareRef, 0)

	for _, dt := range rules() {
		if pos.X+dt.X >= 0 && pos.X+dt.X < b.Size && pos.Y+dt.Y >= 0 && pos.Y+dt.Y < b.Size {
			x := pos.X + dt.X
			y := pos.Y + dt.Y
			res = append(res, squareRef{Position{x, y}, &b.Squares[y][x]})
		}
	}

	return res
}

// moveTo moves pawn to a specified square on the board
func (b *Board) moveTo(pos Position) {
	b.TourSteps = append(b.TourSteps, pos)
	b.Squares[pos.Y][pos.X].StepNumber = len(b.TourSteps)

	for _, sq := range b.adjacentSquares(pos) {
		sq.Ref.Moves--
	}
}

// rollback the board to the state it was in at the specified step
func (b *Board) rollback(step int) {
	if len(b.TourSteps) <= step {
		// Nothing to do here
		return
	}

	for i := len(b.TourSteps) - 1; i >= step; i-- {
		pos := b.TourSteps[i]
		b.Squares[pos.Y][pos.X].StepNumber = 0
		for _, sq := range b.adjacentSquares(pos) {
			sq.Ref.Moves++
		}
	}

	b.TourSteps = b.TourSteps[:step]
}

// minAdjacentSquares returns a list of squares adjacent to specified position with minimal number of moves
func (b Board) minAdjacentSquares(pos Position) []squareRef {
	res := make([]squareRef, 0)

	// Each square can have 8 available moves top, so first square will override this min
	min := 9
	for _, sq := range b.adjacentSquares(pos) {
		if sq.Ref.StepNumber != 0 {
			// This square is part of the tour already
			continue
		}

		if sq.Ref.Moves == min {
			res = append(res, sq)
		} else if sq.Ref.Moves < min {
			// New min
			min = sq.Ref.Moves
			res = res[:0]
			res = append(res, sq)
		}
	}

	return res
}

// isTourComplete checks if pawn has visited all squares on the board
func (b Board) isTourComplete() bool {
	return len(b.TourSteps) == b.Size*b.Size
}

// PrintTour outputs the current tour in stdout
func (b Board) PrintTour() {
	fmt.Println()
	fmt.Println("    Y")

	for y := b.Size - 1; y >= 0; y-- {
		fmt.Printf("%3d |", y)
		for x := 0; x < b.Size; x++ {
			sn := b.Squares[y][x].StepNumber
			if sn == 0 {
				// This square is not part of the tour
				fmt.Print("    -")
			} else {
				fmt.Printf("%5d", sn)
			}
		}
		fmt.Println()
	}

	fmt.Print("     ")
	fmt.Printf("%s X\n", strings.Repeat("-", 5*b.Size+1))
	fmt.Print("     ")
	for i := 0; i < b.Size; i++ {
		fmt.Printf("%5d", i)
	}
	fmt.Println()
	fmt.Println()
}

// TryFindTour makes an attempt to find a tour on a specified board continuing from a specified position.
// Returns true if tour if found, or false otherwise
func (b *Board) TryFindTour(pos Position) bool {
	for {
		b.moveTo(pos)
		if b.isTourComplete() {
			return true
		}

		// Decide where to go next
		candidates := b.minAdjacentSquares(pos)

		if len(candidates) == 0 {
			return false
		}

		if len(candidates) == 1 {
			pos = candidates[0].Pos
		} else {
			for _, sq := range candidates {
				// There are few candidates for next move, try each of them until the tour if found
				step := len(b.TourSteps)
				if b.TryFindTour(sq.Pos) {
					// The tour is found
					return true
				}

				// Couldn't find a tour on this branch, rollback and try other squares
				b.rollback(step)
			}

			return false
		}
	}
}

// readIntFromStdin prompts user to enter an integer positive number using provided label
// and ensures that input number is below specified limit
func readIntFromStdin(label string, limit int) (res int) {
	var input string

	for {
		fmt.Print(label)
		_, err := fmt.Scanln(&input)
		if err != nil {
			fmt.Println(err)
			continue
		}

		res, err = strconv.Atoi(input)
		if err != nil || res < 0 || res >= limit {
			fmt.Printf("Please enter a positive integer number below %d\n", limit)
			continue
		}

		return
	}
}

// getStartPosition prompts user to enter pawn start coordinates on a board of specified side size
func getStartPosition(boardSize int) (x int, y int) {
	fmt.Println("Enter start position")

	x = readIntFromStdin("X: ", boardSize)
	y = readIntFromStdin("Y: ", boardSize)

	return
}

func main() {
	makeGif := flag.Bool("gif", false, "generate a GIF with pawn's steps")
	gifFileName := flag.String("file", "tour.gif", "name of a GIF file to generate, only used with -gif flag")
	flag.Parse()

	// Board side size
	const n = 10

	x, y := getStartPosition(n)

	board := newBoard(n)

	start := time.Now()
	isFound := board.TryFindTour(Position{x, y})
	elapsed := time.Since(start)

	if !isFound {
		fmt.Printf("Can't find a tour for [%d;%d]\n", x, y)
		return
	}

	fmt.Printf("Tour found in %s\n", elapsed)
	board.PrintTour()

	if *makeGif {
		fmt.Printf("Generating GIF at %s... ", *gifFileName)
		generateGif(*gifFileName, board.TourSteps, n)
		fmt.Println("done!")
	}
}
