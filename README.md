# Pawn's Tour

Pawn's Tour is a simple application that finds a path for a pawn to visit all squares
on an [international checkerboard](https://en.wikipedia.org/wiki/International_draughts)
using the following pawn movement rules:
- Jumps 3 squares horizontally and vertically.
- Jumps 2 squares diagonally.
- Jumps to squares already visited or outside of the checkerboard are NOT allowed.

## How to run

You need to have Go installed on your computer to run this app.
You can download Go installation from the [official website](https://golang.org/doc/install)

Once you have Go installed, run:
```
go run ikhlepitko/pawns-tour
```

Use `-gif` flag to generate a GIF file illustrating pawn's tour:
```
go run ikhlepitko/pawns-tour -gif
```

This generates a GIF file called `tour.gif`. You can provide another file name with `-file=name` flag.