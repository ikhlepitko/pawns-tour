package main

import (
	"fmt"
	"testing"
)

func TestAllTours(t *testing.T) {
	const n = 10

	failedCount := 0
	for y := 0; y < n; y++ {
		for x := 0; x < n; x++ {
			board := newBoard(n)
			isFound := board.TryFindTour(Position{x, y})
			if !isFound {
				failedCount++
				fmt.Printf("Tour for [%d;%d] not found\n", x, y)
			}
		}
	}

	if failedCount != 0 {
		t.Fail()
	}
}
